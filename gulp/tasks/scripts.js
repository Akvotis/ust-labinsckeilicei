module.exports = function () {
	var babel = require("gulp-babel"),
		minify = require('gulp-minify'),
		rigger = require('gulp-rigger');

	$.gulp.task('scripts:lib', function () {
		return $.gulp.src(['node_modules/jquery/dist/jquery.min.js', 'node_modules/svgxuse/svgxuse.js', 'node_modules/object-fit-polyfill/dist/object-fit-polyfill.js', 'node_modules/swiper/swiper-bundle.js', 'node_modules/bootstrap/dist/js/bootstrap.min.js', 'node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js', 'node_modules/chart.js/dist/Chart.bundle.min.js'])
			.pipe($.gp.concat('libs.js'))
			.pipe(minify())
			.pipe($.gulp.dest('build/js/'))
			.pipe($.bs.reload({
				stream: true
			}));
	});

	$.gulp.task('scripts', function () {
		return $.gulp.src(['src/dev/js/main.js'])
			.pipe(babel())
			.pipe(minify())
			.pipe(rigger())
			.pipe($.gulp.dest('build/js/'))
			.pipe($.bs.reload({
				stream: true
			}));
	});
}