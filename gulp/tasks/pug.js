module.exports = function() {
	$.gulp.task('pug', function() {
		var rigger = require("gulp-rigger");

		return $.gulp.src('src/pug/pages/*.pug')

		.pipe($.gp.pug({
			pretty: true
		}))
		.pipe(rigger())
		.pipe($.gulp.dest('build'))
		.on('end', $.bs.reload);
	});
}