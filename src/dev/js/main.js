// import 'chartjs-plugin-style';

$(document).ready(function () {
  jQuery.fn.exists = function () {
    return $(this).length;
  }

  function truncateText(bloc, qty) {
    if (bloc.length > 0) {
      let txtBloc = document.querySelectorAll(bloc);
      for (let i = 0; i < txtBloc.length; i++) {
        trc(txtBloc[i], qty);
      }
    }
  }

  function trc(txt, qty) {
    let text = txt.textContent;
    var sliced = text.slice(0, qty);
    if (sliced.length < text.length) {
      sliced += '...';
    }

    txt.textContent = sliced;
  }



  // bootstrap carousel
  $('#myCarousel').carousel({
    interval: 10,
  });

  $(window).on('resize load', function () {
    if ($('.mf-text').exists()) {
      if ($(this).width() <= 620) {
        truncateText('.mf-text', 60);
      }
    }
  });

  $(window).on("load", function () {
    const breakpoint = window.matchMedia('(max-width:601px)');
    if (breakpoint.matches === true) {
      if ($('.navbar-nav').exists()) {
        $(".navbar-nav").mCustomScrollbar({
          axis: "x",
          theme: "dark-3",
          advanced: { autoExpandHorizontalScroll: true }
        });
      }
    }
  });

  // console.log($(".dropdown-table__menu"));

  if ($('.dropdown-table__menu').exists()) {
    $('.dropdown-table__menu').each(function () {
      $(this).mCustomScrollbar({
        axis: 'y', // вертикальный скролл
        theme: "dark-3",
        scrollInertia: '50', // продолжительность прокрутки, значение в миллисекундах
        mouseWheel: {
          deltaFactor: 10 // кол-во пикселей на одну прокрутку колёсика мыши
        },
        autoHideScrollbar: true,
        mouseWheel: { preventDefault: false }
      });
    });


    $('.dropdown-table__menu').bind('mousewheel DOMMouseScroll', function (e) {
      var scrollTo = null;
      if (e.type == 'mousewheel') {
        scrollTo = (e.originalEvent.wheelDelta * -1);
      }
      else if (e.type == 'DOMMouseScroll') {
        scrollTo = 40 * e.originalEvent.detail;
      }
      if (scrollTo) {
        e.preventDefault();
        $(this).scrollTop(scrollTo + $(this).scrollTop());
      }
    });
  }

  if ($('.header__inner').exists) {
    try {
      let $window = $(window),
        $target = $(".header__inner"),
        $h = $target.offset().top;
      $window.on('scroll', function () {
        let scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        if (scrollTop > $h) {
          $target.addClass("mf-fixed");
          return;
        } else {
          $target.removeClass("mf-fixed");
        }
        return;
      });
    } catch (err) {
      console.log(err);
    }
  }


  if ($('.header-section__close').exists()) {
    $('.header-section__close').on('click', () => {
      $('.burger').toggleClass('burger--active');
      $('.header-section').toggleClass('header-section--active');
      $('html').css('overflow', 'auto');

      if (!$('.burger').hasClass('burger--active')) {
        $('.header-section__item').animate({ 'opacity': '0' }, 0.001)
      }

    });
  }

  if ($('.burger').length > 0) {
    $('.burger').on('click', function () {
      $(this).toggleClass('burger--active');
      $('.header-section').toggleClass('header-section--active');
      $('html').css('overflow', 'hidden');


      setTimeout(function () {
        if ($('.header-section__item').exists()) {
          $('.header-section__item').each(function (index) {
            $(this).delay(150 * index).animate({
              "opacity": "1"
            }, 150);
          });
        }
      }, 250);

    });
  }

  if ($('.header-section').exists()) {
    $('.header-section').click(function (e) {
      console.log(e.target);
      if (e.target.className.indexOf('header-section--active') != -1) {
        $('.burger').removeClass('burger--active');
        $('html').css('overflow', 'auto');
        $('.header-section').removeClass('header-section--active');


        if (!$('.burger').hasClass('burger--active')) {
          $('.header-section__item').animate({ 'opacity': '0' }, 0.001);
        }
      }
    });
  }

  // swiper
  var mySwiper = new Swiper('.swiper-container', {

    slidesPerView: 2,
    spaceBetween: 20,
    loop: true,
    speed: 500,

    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },

    breakpoints: {
      320: {
        slidesPerView: 1.6,
        spaceBetween: 15,
        centeredSlides: true,
      },

      501: {
        slidesPerView: 3,
        spaceBetween: 15,
        centeredSlides: true,
      },

      970: {
        slidesPerView: 3,
        spaceBetween: 15,
      },
      1100: {
        slidesPerView: 4,
        spaceBetween: 15,
      },

      1500: {
        slidesPerView: 4,
        spaceBetween: 20,
      }
    },
  });

  if ($('.desc-slider').exists()) {
    let chart2 = new Swiper('.desc-slider', {
      // pagination: '.swiper-pagination',
      slidesPerView: 2,
      // spaceBetween: 54,
      navigation: {
        nextEl: '.arr-nexts',
        prevEl: '.arr-prevs'
      },
      breakpoints: {
        320: {
          slidesPerView: 1,
          spaceBetween: 30,
        },

        769: {
          slidesPerView: 1,
          spaceBetween: 30,
        },
        1236: {
          slidesPerView: 2,
          spaceBetween: 30,
        },

        1920: {
          slidesPerView: 2,
          spaceBetween: 54,
        }
      },
      // paginationClickable: true
    })
  }

  if ($('.b-surveyResult-chart--chart').exists()) {
    let chart = new Swiper('.b-surveyResult-chart--chart', {
      // pagination: '.swiper-pagination',
      slidesPerView: 2,
      spaceBetween: 54,

      navigation: {
        nextEl: '.arr-next',
        prevEl: '.arr-prev'
      },
      breakpoints: {
        320: {
          slidesPerView: 1,
          spaceBetween: 20,
        },

        769: {
          slidesPerView: 2,
          spaceBetween: 30,
        },
        1025: {
          slidesPerView: 2,
          spaceBetween: 30,
        },

        1920: {
          slidesPerView: 2,
          spaceBetween: 54,
        }
      },
      // paginationClickable: true
    })
  }

  // active class
  // news navbar active
  if ($('.nav-links').exists()) {
    $('.nav-links').on('click', function () {
      $(this).toggleClass('nav-links--active');
    })
  }

  //rotate svg accordion
  if ($('.card-header a').exists()) {
    $('.card-header a').on('click', function () {
      $(this).closest('.card-header').toggleClass('card-header--rotate');
    })
  }

  // active class menu
  if ($('.navbar-header .nav-item').exists()) {
    $('.navbar-header .nav-item').on('click', function () {
      if ($(this).siblings('.nav-item').hasClass('nav-link--active')) {
        $('.nav-item').removeClass('nav-link--active')
      }
      $(this).addClass('nav-link--active');
    })
  }

  // active class burger menu
  if ($('.menu__box').exists()) {
    $('.menu__box').find('li').on('click', function () {
      $(this).siblings().removeClass('menu__item--active');
      $(this).addClass('menu__item--active');
    })
  }


  // collapse 
  if ($('button').exists()) {
    $('button').each(function () {
      $(this).on('click', function () {
        let temp = $(this).data('target');

        $(this).siblings().removeClass('btn-active');
        $(this).addClass('btn-active');

        $('.b-collapse').each(function () {
          if ($(this).data('target') == temp) {
            console.log($(this).siblings('b-collapse'));

            $(this).siblings().removeClass('b-collapse--active');
            $(this).addClass('b-collapse--active');
          }
        });
      });
    });
  }


  // accordion
  // admin structure
  if ($('.b-accordion-third').exists()) {
    $('.b-accordion-third').find('a').each(function () {
      $(this).on('click', function () {

        $(this).siblings().removeClass('b-accordion-third--active');
        $(this).addClass('b-accordion-third--active');

      });
    })
  }

  if ($('#accordion a').exists()) {
    $('#accordion a').click(function () {
      $(this).find('i').toggleClass('fa-caret-down--up');
    })
  }

  // department
  if ($('.collapse').exists()) {
    $('.collapse').on('click', function () {
      $(this).siblings().removeClass('card-body--active');
      $(this).addClass('card-body--active');
    })
  }

  // contact
  if ($('.accordion-card').exists()) {

    $('.accordion-card').each(function () {
      $(this).on('click', function () {
        let acCont = $(this).find(".accordion-content");
        let acBox = $(this).find(".accordion-box");

        if (!$(this).hasClass('accordion-card--active')) {
          $(this).addClass('accordion-card--active');
          let heightCont = $(acCont).innerHeight();
          $(acBox).css('max-height', heightCont);


        } else {

          $(acBox).css('max-height', '0');
          $(this).removeClass('accordion-card--active');
          console.log($(acBox));
        }
      });
    });
  }


  // table dropdown
  if ($('.th-active').exists()) {
    $('.th-active').each(function () {
      $(this).on('click', function () {

        $(this).siblings().removeClass('th-active--show');
        $(this).toggleClass('th-active--show');

      }
      );
    });
  }

  if ($('.dropdown-table__menu a').exists()) {
    $('.dropdown-table__menu a').each(function () {
      $(this).on('click', function () {

        let text = $(this).text();
        $(this).parents().siblings('.btn-table').text(text);

      })
    })
  }

  if (($('.btn-table') && $('.dropdown-table__menu')).exists()) {
    $(document).on('click', function (e) {
      const button = $(this).find('.btn-table'),
        dropMenu = $(this).find('.dropdown-table__menu');

      if (!button.is(e.target) && !dropMenu.is(e.target)) {

        $('.th-active').removeClass('th-active--show');

      }
    })
  }

  //admin structure switch content
  if ($('.b-senate__item').exists()) {
    $('.b-senate__item').on('click', function () {
      $('.adminStructure').toggleClass('adminStructure--active');

      $(this).siblings().removeClass('b-senate__item--active');
      $(this).toggleClass('b-senate__item--active');
    })
  }

  // news dropdown 
  if ($('.b-news-dropdown').exists()) {
    $('.b-news-dropdown').on('click', function () {
      $(this).toggleClass('b-news-dropdown--active');
    })

    $(document).on('click', function (e) {
      const btn = $(this).find('.b-btn-sort'),
        dropList = $(this).find('.b-news-dropdown__menu');

      if (!btn.is(e.target) && !dropList.is(e.target)) {
        $('.b-news-dropdown').removeClass('b-news-dropdown--active');
      }
    })

    $('.b-news-dropdown__menu a').on('click', function () {
      let text = $(this).text();
      $(this).parents().siblings('.b-btn-sort').text(text);
    })
  }

  // popup
  //open reception
  if ($('.b-reception__button').exists()) {
    $('.b-reception__button').on('click', function () {
      $('.popup').fadeIn();
    })

    //closing when click on the png
    $('.b-popup__close').click(function () {
      $(this).parents('.popup').fadeOut();
      return false;
    })

    //closing when click on the png
    $('.b-popup__close--dark').click(function () {
      $(this).parents('.popup').fadeOut();
      return false;
    })

    //closing when click on the btn
    $('.b-news__button--popup').on('click', function () {
      $('.popup').fadeOut();
    })

    // closing when keydown on the Esc
    $(document).keydown(function (e) {
      if (e.keyCode === 27) {
        e.stopPropagation();
        $('.popup').fadeOut();
      }
    })

    // closing when click on the background
    $('.popup').click(function (e) {
      if ($(e.target).closest('.b-popup').length == 0) {
        $(this).fadeOut();
      }
    })
  }

  //close and open popup
  if ($('.b-survey-body__item').exists()) {
    $('.b-survey-body__item').each(function () {
      $(this).on('click', function () {
        let idPopup = $(this).data('popup');
        $(idPopup).addClass('showPopup');
        $('body').addClass('openPopup');
      });
    });

    //close x
    $('.b-popup__close').click(function () {
      $(this).parents('.surveyPopup').removeClass('showPopup');
      $('body').removeClass('openPopup');
      return false;
    })

    $('.b-popup__close--dark').click(function () {
      $(this).parents('.surveyPopup').removeClass('showPopup');
      $('body').removeClass('openPopup');
      return false;
    })

    // closing when keydown on the Esc
    $(document).keydown(function (e) {
      if (e.keyCode === 27) {
        e.stopPropagation();
        $('.surveyPopup').removeClass('showPopup');
        $('body').removeClass('openPopup');
      }
    })

    // closing when click on the background
    $('.surveyPopup').click(function (e) {
      if ($(e.target).closest('.b-surveyPopup').length == 0) {
        $(this).removeClass('showPopup');
        $('body').removeClass('openPopup');
      }
    })
  }

  // close popup result
  // close x
  if ($('.b-popup__close').exists()) {
    $('.b-popup__close').click(function () {
      $(this).parents('.surveyResult').removeClass('showPopup');
      return false;
    })

    $('.b-popup__close--dark').click(function () {
      $(this).parents('.surveyResult').removeClass('showPopup');
      return false;
    })

    // click button 
    $('.b-surveyResult__btn').on('click', function () {
      $('.surveyResult').removeClass('showPopup');
    })

    // closing when keydown on the Esc
    $(document).keydown(function (e) {
      if (e.keyCode === 27) {
        e.stopPropagation();
        $('.surveyResult').removeClass('showPopup');
      }
    })

    // closing when clicked on background
    $('.surveyResult').click(function (e) {
      if ($(e.target).closest('.b-surveyResult').length == 0) {
        $(this).removeClass('showPopup');
        $('body').removeClass('openPopup');
      }
    })
  }

  // survey popup III dropdown
  if ($('.form').exists()) {
    $('.b-surveyPopup__form .form-droplist').not('.form--active + .form-droplist').hide();
    $('.b-surveyPopup__form .form').on('click', function () {

      if ($(this).hasClass('form--active')) {
        $(this).removeClass('form--active');
        $(this).next().slideUp(300);
      } else {
        $('.b-surveyPopup__form .form').removeClass('form--active');
        $(this).addClass('form--active');
        $('.b-surveyPopup__form .form-droplist').slideUp(300);
        $(this).next().slideDown(300);
      }
    })

    $('.form-droplist a').each(function () {
      $(this).on('click', function () {
        let text = $(this).text();

        $(this).parent().prev().find('input[type="text"]').val(text);
        $('.b-surveyPopup__form .form').next().slideUp(300);
      })
    })
  }

  // chart.js
  if ($('.circle--chart').exists()) {
    let chart = document.querySelectorAll('.circle--chart');

    chart.forEach((item, i) => {
      let ctx = item.getContext('2d');
      let dataLabels = item.getAttribute("data-labels");
      let dataNumber = item.getAttribute("data-number");
      let dataColor = item.getAttribute("data-color");

      function getFontColor(colors) {
        let arrColor = colors,
          newArrColor = [];

        function hexToRgb(hex) {
          let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
          return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
          } : null;
        }

        arrColor.forEach((item, i) => {
          let rbgColor = hexToRgb(item).g;
          if (rbgColor < 125) {
            newArrColor.push('#ffffff');
          } else if (rbgColor > 125) {
            newArrColor.push('#333333');
          }
        })
        return newArrColor;
      }

      let circle = new Chart(ctx, {
        type: 'pie',
        data: {
          labels: JSON.parse(dataLabels),
          datasets: [{
            data: JSON.parse(dataNumber),
            backgroundColor: JSON.parse(dataColor),
            borderWidth: 0,
            borderColor: 'inherit',
          }]
        },
        options: {
          responsive: true,
          legend: {
            display: true,
            position: 'bottom',
            align: 'left',
            labels: {
              boxWidth: 13,
              fontColor: '#3B3B3B',
              padding: 15,
              fontSize: 14,
              lineHeght: 1.75,
              fontWeght: 500
            }
          },
          tooltips: {
            enabled: false
          },
          plugins: {
            datalabels: {
              textAling: 'center',
              // color: '#FFFFFF',
              textShadowColor: 'black',
              font: {
                lineHeght: 1.75,
                size: 16
              },
              formatter: function (value) {
                return value + '%';
              },
              color: getFontColor(JSON.parse(dataColor))
            }
          }
        },
      })
    });
  }

  if ($('.bar--chart').exists()) {
    let chart = document.querySelectorAll('.bar--chart');

    chart.forEach((item, i) => {
      let ctx = item.getContext('2d');
      let dataLabels = item.getAttribute("data-labels");
      let dataNumber = item.getAttribute("data-number");
      let dataColor = item.getAttribute("data-color");
      let dataName = item.getAttribute("data-name");

      // ctx.canvas.width = 300;
      // ctx.canvas.height = 300;

      let bar = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: JSON.parse(dataLabels),
          datasets: [{
            label: 'Опрос под номером 1',
            data: JSON.parse(dataNumber),
            backgroundColor: JSON.parse(dataColor)
          }],
        },
        options: {
          responsive: true,
          legend: {
            display: false,
            position: 'bottom',
            align: 'center',
            labels: {
              boxWidth: 13,
              fontColor: '#3B3B3B',
              padding: 15,
              fontSize: 14,
              lineHeght: 1.75,
              fontWeght: 500,
              fontColor: '#A5881A'
            }
          },
          plugins: {
            datalabels: false
          },
          tooltips: {
            callbacks: {
              label: function (tooltipItem, data) {
                var value = data.datasets[0].data[tooltipItem.index];
                value = value.toString();
                value = value.split(/(?=(?:...)*$)/);
                value = value.join(',');
                console.log(value)
                return value;
              }
            }
          },
          scales: {
            xAxes: [{
              barPercentage: 0.5,
              categoryPercentage: 0.6,
              gridLines: {
                offset: true
              }
            }]
          }
        },
      })
    })
  }

  // reception dropdown
  if ($('.b-reception-form__dropdown').exists()) {
    if ($('.b-reception-form__dropdown').exists()) {
      $('.b-reception-form__dropdown').on('click', function () {
        $(this).toggleClass('b-reception-form__dropdown--active');
      })
    }

    $(document).on('click', function (e) {
      const droplist = $(this).find('.b-reception-form__list'),
        btn = $(this).find('input[type="select"]');

      if (!btn.is(e.target) && !droplist.is(e.target)) {
        $('.b-reception-form__dropdown').removeClass('b-reception-form__dropdown--active');
      }
    })


    if ($('.b-reception-form__list').exists()) {
      $('.b-reception-form__list a').each(function () {
        $(this).on('click', function () {
          let text = $(this).text();
          $('input[type="select"]').val(text);
        })
      })
    }
  }
})